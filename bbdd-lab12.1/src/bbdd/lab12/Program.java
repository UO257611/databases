package bbdd.lab12;
import java.sql.*;
import java.util.Scanner;
import org.hsqldb.types.Types;
public class Program {
	
	
	
	//ORACLE
	/*
	private static String  USERNAME = "UO257611";
	private static String PASSWORD = psp.password;	
	private static String CONNECTION_STRING = "jdbc:oracle:thin:@156.35.94.99:1521:DESA";
	
	
	*/
	//HSQLDB
	private static String  USERNAME = "SA";
	private static String PASSWORD = "";	
	private static String CONNECTION_STRING = "jdbc:hsqldb:hsql://localhost/labdb";

	
	
	public static void main(String[] args) {
		//Examples to read by keyboard
		//System.out.println("Read an integer by keyboard");	
		//int integ = ReadInt();
		//System.out.println("Read a string by keyboard");	
		//String str = ReadString();
		try
		{
			exercise8();
		}catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/*
		1. Show the results of queries 21 and 32 from lab session 2.
			21. Name and surname of clients that have bought a car in
			a �Madrid� dealer that has �gti� cars.
	 */
	public static void exercise1_1() throws SQLException
	{
		
		Connection conn = getConnection();	
		StringBuilder query = new StringBuilder();
		query.append("SELECT nombre, apellido ");
		query.append("FROM clientes ");
		query.append("WHERE dni IN ( ");
		query.append("SELECT DISTINCT  V.dni ");
		query.append("FROM ventas V, coches CH, concesionarios CO, distribucion D\r\n ");
		query.append("WHERE V.cifc=CO.cifc ");
		query.append("AND CO.ciudadc='madrid'");
		query.append("	AND CO.cifc=D.cifc ");		 
		query.append("AND D.codcoche = CH.codcoche ");
		query.append("AND CH.modelo='gti'");
		query.append(" ); ");
		
		Statement statement = conn.createStatement();
		ResultSet rs = statement.executeQuery(query.toString());
		
		showResults(rs);
		
		rs.close();
		statement.close();
		conn.close();
	}
	
	
	private static void showResults(ResultSet rs ) throws SQLException
	{
		int columnCount = rs.getMetaData().getColumnCount();
		StringBuilder headers = new StringBuilder();
		for(int i = 1; i< columnCount; i++)
		{
			headers.append( rs.getMetaData().getColumnName(i) +" \t");
		}
		System.out.println(headers.toString());
		
		StringBuilder result = null;
		while(rs.next())
		{
			result = new StringBuilder();
			for(int i = 1; i< columnCount; i++)
			{
				result.append(rs.getObject(i) + "\t");
			}
			result.append(rs.getObject(columnCount));
			System.out.println(result.toString());
			
		}
		
		if(result == null)
			System.out.println("Not data found");
	}
	
	/* 
			32. Dealers having an average stockage greater than the
			average stockage of all dealers together. 
	*/
	public static void exercise1_2() throws SQLException 
	{
		Connection conn = getConnection();	
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT CIFC,nombrec , ciudadc ");
		query.append("FROM DISTRIBUCION D, CONCESIONARIOS C ");
		query.append("WHERE D.CIFC = C.CIFC ");
		query.append("GROUP BY D.CIFC, C.nombrec, C.ciudadc ");
		query.append("HAVING avg ( CANTIDAD) > ( ");
		query.append("SELECT avg(CANTIdad) ");
		query.append("FROM DISTRIBUCION ");
		query.append(");");
		
		Statement statement = conn.createStatement();
		ResultSet rs = statement.executeQuery(query.toString());
		
		showResults(rs);
		
		rs.close();
		statement.close();
		conn.close();
		
	}
	
	/*
		2. Show the results of query 6 from lab session 2, so that the
		search color is inputted by the user.
			6. Names of car makers that have sold cars with a color that
			is inputted by the user
	*/
	public static void exercise2() throws SQLException 
	{
		String input = ReadString();
		
		Connection conn = getConnection();
		StringBuilder query = new StringBuilder();
		
		query.append("SELECT DISTINCT M.nombrem ");
		query.append("FROM marcas M, marco R, ventas V ");
		query.append("WHERE M.cifm=R.cifm ");
		query.append("AND R.codcoche=V.codcoche ");
		query.append("AND V.color= ? ;");
		 	
		PreparedStatement pst = conn.prepareStatement(query.toString());
		pst.setString(1, input);
		
		ResultSet rs = pst.executeQuery();
		
		showResults(rs);
		
		rs.close();
		pst.close();
		conn.close();
		
	}
	
	/*
		3. Show the results of query 27 from lab session 2, so that the
		limits for the number of cars are inputted by the user.
			27. Cifc of dealers with a stock between two values inputted
			by the user inclusive.
	 */
	public static void exercise3() throws SQLException {
		System.out.println("The input for the lower bound: ");
		String input1 = ReadString();

		System.out.println("The input for the lower bound: ");
		String input2 = ReadString();

		Connection conn = getConnection();
		StringBuilder query = new StringBuilder();
		query.append("SELECT CIFC, SUM(CANTIDAD) CATIDADES ");
		query.append("FROM DISTRIBUCION ");
		query.append("GROUP BY CIFC ");
		query.append("HAVING sum(CANTIDAD)>= ? AND SUM(CANTIDAD)<= ? ;");
		
		PreparedStatement pst = conn.prepareStatement(query.toString());
		pst.setString(1, input1);
		pst.setString(2, input2);
		
		ResultSet rs = pst.executeQuery();
		showResults(rs);
		
		
		rs.close();
		pst.close();
		conn.close();
		
				
				
				
				
		
	}
	
	/*
		4. Show the results of query 27 from lab session 2, so that the city
		of the dealer and the color are inputted by the user.
			24. Names of the clients that have NOT bought cars with a
			user-defined color at dealers located in a user-defined city.
	 */
	public static void exercise4() {
		
	}
	
	/*
		5. Develop a Java method that, using the suitable SQL sentence:
			5.1 Creates cars into the COCHES table, taking the data from the user.
	 */
	public static void exercise5_1 () 
	{
		try
		{
		int codcoche = ReadInt();
		String namebranch= ReadString();
		String modelo = ReadString();


		Connection conn = getConnection();
		StringBuilder query = new StringBuilder();
		
		query.append("INSERT INTO coches ");
		query.append("VALUES ( ?, ?, ? )");
	
		
		PreparedStatement pst = conn.prepareStatement(query.toString());
		pst.setInt(1, codcoche);
		pst.setString(2, namebranch);
		pst.setString(3, modelo);
		
		if(pst.executeUpdate() == 1)
		{
			System.out.println("Success");
		}
		else
		{
			System.out.println("Fail");
		}
		
		
		
		
		pst.close();
		conn.close();
		}catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("pa k quieres saber eso jajaja salu2");
		}
		
		
		
	}
	
	/*
			5.2. Deletes a specific car. The code for the car to delete is
			defined by the user. 
	 */
	public static void exercise5_2() throws SQLException 
	{
		int cod = ReadInt();
		Connection con = getConnection();
		StringBuilder query = new StringBuilder();
		
		query.append("DELETE FROM coches ");
		query.append("WHERE codcoche = ? ");
		PreparedStatement pst = con.prepareStatement(query.toString());
		pst.setInt(1, cod);
		
		if(pst.executeUpdate() == 1)
		{
			System.out.println("Success");
		}
		else
		{
			System.out.println("Fail");
		}
		
		pst.close();
		con.close();
		
		
	}
	
	/*	 
			5.3. Updates the name and model for a specific car. The code for
			the car to update is defined by the user
	 */
	public static void exercise5_3() throws SQLException 
	{
		int cod = ReadInt();
		String name = ReadString();
		String model = ReadString();
		
		Connection con = getConnection();
		StringBuilder query = new StringBuilder();
		
		query.append("UPDATE coches ");
		query.append("SET nombrech = ?, modelo = ?");
		query.append("Where codcoche = ?");
		
		PreparedStatement pst = con.prepareStatement(query.toString());
		pst.setString(1, name);
		pst.setString(2, model);
		pst.setInt(3, cod);
		
	
		
		if(pst.executeUpdate() == 1)
		{
			System.out.println("Success");
		}
		else
		{
			System.out.println("Fail");
		}
		
		pst.close();
		con.close();
		
		
		
	}
	
	/*
		6. Invoke the exercise 10 function and procedure from lab session 10
		from a Java application.
			10. Develop a stored procedure that takes a dealer id, and then
			returns the number of sales that were made in that dealer. Develop
			a function with the same functionality.
		6.1. Function
	 */
	public static void exercise6_1() throws SQLException
	{		
			if(!CONNECTION_STRING.contains("oracle") ) return ;
			Connection con = getConnection();
			String cifc = ReadString();
			StringBuilder query = new StringBuilder();
			query.append("{ ? = call elena.FnumVentasConcesionario(?)}");
			CallableStatement cst = con.prepareCall(query.toString());
			cst.registerOutParameter(1, Types.INTEGER);
			cst.setString(2, cifc);
			cst.execute();
			int ventas = cst.getInt(1);
			System.out.println("Ventas : "+ ventas);
			cst.close();
			con.close();
	}
	
	
	/*	
		6.1. Procedure
	 */
	public static void exercise6_2() throws SQLException 
	{
		if(!CONNECTION_STRING.contains("oracle") ) return ;
		Connection con = getConnection();
		String cifc = ReadString();
		StringBuilder query = new StringBuilder();
		query.append("{ call elena.PnumVentasConcesionario(?,?)}");
		CallableStatement cst = con.prepareCall(query.toString());
		cst.registerOutParameter(2, Types.INTEGER);
		cst.setString(1, cifc);
		cst.execute();
		int ventas = cst.getInt(2);
		System.out.println("Ventas : "+ ventas);
		cst.close();
		con.close();		
	}
	
	/*
		7. Invoke the exercise 11 function and procedure from lab session 10
		from a Java application.
			11. Develop a PL/SQL function called ClientesPorCiudad (customers
			per city) that takes a city and then returns the number of customers
			in that city. Develop a procedure with the same functionality.
		7.1. Function
	 */
	public static void exercise7_1() throws SQLException 
	{
		if(!CONNECTION_STRING.contains("oracle") ) return ;
		Connection con = getConnection();
		String cifc = ReadString();
		StringBuilder query = new StringBuilder();
		query.append("{ ? = call elena.Fclientesporciudad(?)}");
		CallableStatement cst = con.prepareCall(query.toString());
		cst.registerOutParameter(1, Types.INTEGER);
		cst.setString(2, cifc);
		cst.execute();
		int ventas = cst.getInt(1);
		System.out.println("Clientes por ciudad: "+ ventas);
		cst.close();
		con.close();				
	}	
	
	/*
		7.2. Procedure
	 */
	public static void exercise7_2() throws SQLException 
	{		
	
		if(!CONNECTION_STRING.contains("oracle") ) return ;
		Connection con = getConnection();
		String cifc = ReadString();
		StringBuilder query = new StringBuilder();
		query.append("{ call elena.Pclientesporciudad(?,?)}");
		CallableStatement cst = con.prepareCall(query.toString());
		cst.registerOutParameter(2, Types.INTEGER);
		cst.setString(1, cifc);
		cst.execute();
		int ventas = cst.getInt(2);
		System.out.println("Clientes por ciudad: "+ ventas);
		cst.close();
		con.close();	
		
	}
	
	/*
		8. Develop a Java method that shows the same information that the
		procedure of exercise 3 from the lab session 11. Hence, it must
		display the cars that have been bought by each customer. Besides, it must
		display the number of cars that each customer has bought and the number of
		dealers where each customer has bought. 
	 */
	public static void exercise8() throws SQLException 
	{
		
		Connection con = getConnection();
		StringBuilder query = new StringBuilder(), query2 = new StringBuilder();
		
		query.append("select dni, nombre, apellido  " );
		query.append("from CLIENTES");

		query2.append("Select c.codcoche, c.nombrech, c.modelo ");
		query2.append("From Ventas v, Coches c ");
		query2.append("Where v.dni = ? and c.codcoche = v.codcoche");
		
		
		PreparedStatement pst = con.prepareStatement(query.toString());
		PreparedStatement pst2 = con.prepareStatement(query.toString());
		
		ResultSet rs = pst.executeQuery();
		
		while (rs.next())
		{
			int columnCount = rs.getMetaData().getColumnCount();
			StringBuilder result = new StringBuilder();
			for(int i = 1; i< columnCount; i++)
			{
				result.append(rs.getObject(i) + "\t");
			}
			System.out.println(result);
			
			query= new StringBuilder();
			
			
			

		}
		
		
	}
		
	@SuppressWarnings("resource")
	private static String ReadString(){
		return new Scanner(System.in).nextLine();		
	}
	
	@SuppressWarnings("resource")
	private static int ReadInt(){
		return new Scanner(System.in).nextInt();			
	}	
	
	private static Connection getConnection() throws SQLException
	{
		if(DriverManager.getDriver(CONNECTION_STRING )== null)
		{
			if (CONNECTION_STRING.contains("oracle"))
			{
				DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
			}
			else
			{
				DriverManager.registerDriver(new org.hsqldb.jdbc.JDBCDriver());
				
			}
			
		}
		return DriverManager.getConnection(CONNECTION_STRING,USERNAME,PASSWORD);
	}
	
}
