package bbdd.lab12;

import java.sql.*;
import java.util.Scanner;

import org.hsqldb.types.Types;

public class ExamPractice
{
	
	private static String CONNECTION_STRING = "jdbc:hsqldb:hsql://localhost/labdb";
	private static String USER = "SA";
	private static String PASSWORD ="";
	
	
	public static void main(String[] arg) 
	{
		try 
		{
		exercise2();
		}
		catch (SQLException e)
		{
			System.out.println("You just got prank");
			e.printStackTrace();
		}
	}
	
	
	public static Connection getConnection() throws SQLException
	{
		if( DriverManager.getDriver(CONNECTION_STRING) == null)
		{
			if(CONNECTION_STRING.contains("oracle"))
				DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
			else
				DriverManager.registerDriver(new org.hsqldb.jdbc.JDBCDriver());
				
		}
		
		return DriverManager.getConnection(CONNECTION_STRING,USER,PASSWORD);
	}
	
	/*1. Show the results of queries 21 and 32 from lab session 2.
	21. Name and surname of clients that have bought a car in
	a �Madrid� dealer that has �gti� cars.*/
	public static void exercise1_1() throws SQLException
	{
		Connection conn = getConnection();
		
		StringBuilder query = new StringBuilder();
		
		
		query.append("Select l.nombre, l.apellido ");
		query.append("From Ventas v, Concesionarios c, Clientes l , Coches o ");
		query.append("Where c.cifc = v.cifc and c.ciudadc ='madrid' and v.dni = l.dni "
				+ "	and o.codcoche = v.codcoche and o.modelo='gti' ");
		
		
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(query.toString());
		
		showResult(rs);
		
		rs.close();
		st.close();
		conn.close();
		
		
		
	}

	private static void showResult(ResultSet rs) throws SQLException {
		int columnCount = rs.getMetaData().getColumnCount();
		StringBuilder headers = new StringBuilder();
		StringBuilder results = new StringBuilder();
		for(int i = 1; i<= columnCount; i++)
		{
			headers.append(rs.getMetaData().getColumnName(i)+ "\t");
		}
		
		while(rs.next())
		{
			for(int i= 1; i<= columnCount; i++)
			{
				results.append(rs.getObject(i)+"\t");
			}
			results.append("\n");
		}
		
		System.out.println(headers.toString());
		System.out.println(results.toString());
		
	}
	
	/* 
	32. Dealers having an average stockage greater than the
	average stockage of all dealers together. 
	*/
	public static void  exercise1_2() throws SQLException
	{
		
		Connection conn = getConnection();
		
		StringBuilder query = new StringBuilder();
		
		query.append("Select c.cifc, c.nombrec ");
		query.append("From Concesionarios c, Distribucion d ");
		query.append("Where c.cifc = d.cifc ");
		query.append("Group by c.cifc ");
		query.append("Having avg(d.cantidad) > ");
		query.append("(Select avg(Cantidad) ");
		query.append("From Distribucion ) ");
		
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(query.toString());
		showResult(rs);
		st.close();
		rs.close();
		conn.close();		
		
	}
	
	/*
	2. Show the results of query 6 from lab session 2, so that the
	search color is inputted by the user.
		6. Names of car makers that have sold cars with a color that
		is inputted by the user
	*/
	public static void exercise2() throws SQLException
	{
		Connection conn = getConnection();
		StringBuilder query = new StringBuilder();
		System.out.println("Write the desire color");
		String color = ReadString();
		
		query.append("Select m.nombrem ");
		query.append("From Marcas m, Marco a, Ventas v ");
		query.append("Where v.codcoche = a.codcoche and a.cifm = m.cifm "
				+ "and v.color = ? ");
		PreparedStatement pt = conn.prepareStatement(query.toString());
		pt.setString(1, color);
		ResultSet rs = pt.executeQuery();
		showResult(rs);
		rs.close();
		pt.close();
		conn.close();
		
	}
	
	
	@SuppressWarnings("resource")
	private static String ReadString(){
		return new Scanner(System.in).nextLine();	
	}
	
	
	

}
